const { APP_HEROKU_URL } = process.env;

module.exports = ({ env }) => ({
  url: env('PUBLIC_URL', APP_HEROKU_URL),
});
