const { SERVER_HOST, SERVER_PORT } = process.env;

module.exports = ({ env }) => ({
  host: env('HOST', SERVER_HOST || 'localhost'),
  port: env.int('PORT', SERVER_PORT || 1337),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', 'd9d3a4ba3a7cfd1102836445d28af69c'),
    },
  },
});
